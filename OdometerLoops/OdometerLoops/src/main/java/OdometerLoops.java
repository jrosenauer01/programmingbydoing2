
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class OdometerLoops {

    public static void main(String[] args) throws Exception {
        Scanner keyboard = new Scanner(System.in);
        int base = 0;
        System.out.println("what base? (2-10): ");
        base = keyboard.nextInt();

        for (int thous = 0; thous < base; thous++) {
            for (int hund = 0; hund < base; hund++) {
                for (int tens = 0; tens < base; tens++) {
                    for (int ones = 0; ones < base; ones++) {
                        System.out.print(" " + thous + "" + hund + "" + tens + "" + ones + "\r");
                        Thread.sleep(10);
                    }
                    System.out.println();
                }
            }
