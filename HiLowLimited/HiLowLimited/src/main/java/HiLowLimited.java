
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class HiLowLimited {

    public static void main(String[] args) {

        Random r = new Random();
        Scanner keyboard = new Scanner(System.in);

        int n = 1 + r.nextInt(100);
        int guess = 0;
        int tries = 1;

        System.out.println("I'm thinking of a number between 1-100. Try to guess.");
        System.out.println("First guess: ");
        guess = keyboard.nextInt();

        while (guess != n && tries < 7) {
            if (guess < n) {
                System.out.println("Sorry you are too low.");
            }
            if (guess > n) {
                System.out.println("Sorry you are too high.");
            }

            tries++;

            System.out.println("Guess #" + tries + ":");
            guess = keyboard.nextInt();
        }

        if (guess != n) {
            System.out.println("Sorry you did not guess in 7 tries. You lose");
        }
        if (guess == n) {
            System.out.println("You guessed it! What are the odds?!");
        }

    }
}
