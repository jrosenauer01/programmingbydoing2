
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Keychains3 {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int choice = 1;
        int numKeychains = 0;
        int price = 10;
        int shipping = 5;
        int extraShipping = 1;
        double salesTax = 8.25;

        System.out.println("Ye Olde Keychain Shoppe");
        System.out.println("");

        while (choice != 4) {
            System.out.println("1. Add Keychains to Order");
            System.out.println("2. Remove Keychains from Order");
            System.out.println("3. View Current Order");
            System.out.println("4. Checkout");
            System.out.println();
            System.out.println("Please enter your choice: ");
            choice = keyboard.nextInt();
            System.out.println();

            if (choice == 1) {
                numKeychains = addKeychains(numKeychains);
                System.out.println("You have " + numKeychains + " keychains.");
            } else if (choice == 2) {
                numKeychains = removeKeychains(numKeychains);
                System.out.println("You now have " + numKeychains + " keychains.");
            } else if (choice == 3) {
                viewOrder(numKeychains, price, shipping, extraShipping, salesTax);
            } else if (choice == 4) {
                checkout(numKeychains, price, shipping, extraShipping, salesTax);
            } else {
                System.out.println("Please try again.");
            }

            System.out.println();
        }
    }

    public static int addKeychains(int keychains) {
        int numKeychains = 0;
        System.out.println("You have " + numKeychains + " keychains. How many to add?");
        Scanner keyboard = new Scanner(System.in);
        int add = keyboard.nextInt();
        keychains += add;
        return keychains;
    }

    public static int removeKeychains(int keychains) {
        int numKeychains = 0;
        Scanner keyboard = new Scanner(System.in);

        do {
            System.out.println("You have " + numKeychains + " keychains. How many to remove?");
            int remove = keyboard.nextInt();
            if (remove > keychains) {
                System.out.println("You cannot remove that many! Try again.");
            }
            while (remove > keychains);
            keychains -= remove;
            return keychains;
        }
        
    }

    public static void viewOrder(int num, int cost, int shipping, int extraShipping, double salesTax) {
        int total = num * cost;
        int totalShipping = shipping + (num - 1) * (extraShipping);
        int withShipping = total + totalShipping;
        double finalCost = withShipping * salesTax / 100 + withShipping;
        System.out.println("You have " + num + " keychains.");
        System.out.println("Keychains cost $" + cost + " each.");
        System.out.println("Shipping charges amount to $" + totalShipping);
        System.out.println("Total cost before tax is $" + withShipping);
        System.out.println("Total cost after tax is $" + finalCost);
    }

    public static void checkout(int num, int cost, int shipping, int extraShipping, double salesTax) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = keyboard.next();
        viewOrder(num, cost, shipping, extraShipping, salesTax);
        System.out.println("Thank you for your order, " + name);
    }

}

