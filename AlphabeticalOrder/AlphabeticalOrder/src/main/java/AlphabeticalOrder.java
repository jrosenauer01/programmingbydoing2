
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AlphabeticalOrder {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        String name;

        System.out.println("What is your last name?");
        name = keyboard.next();

        int a = name.compareTo("Carswell");
        int b = name.compareTo("Jones");
        int c = name.compareTo("Smith");
        int d = name.compareTo("Young");

        if (a <= 0) {
            System.out.println("You don't have to wait long.");

        } else if (b <= 0) {
            System.out.println("That's not bad.");

        } else if (c <= 0) {
            System.out.println("It's going to be a bit of a wait.");

        } else if (d <= 0) {
            System.out.println("It's going to be a while");
        } else {
            System.out.println("Not going anywhere for a while.");
        }
    }
}
