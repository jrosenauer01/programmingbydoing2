
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class BasicArrays0 {

    public static void main(String[] args) {

        List<String> numList = new ArrayList<>();

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The first number");
        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The first number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The second number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The third number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The fourth number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The fifth number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The sixth number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The seventh number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The eighth number");

        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The ninth number");
        
        System.out.println("Slot " + numList.size() + " contains a -113");
        numList.add("The tenth number");
    }
}
