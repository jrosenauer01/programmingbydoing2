
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class CountingFor {
    public static void main (String [] args) {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Type in a message,and I'll display it five times.");
        System.out.println("Message: ");
        String message = keyboard.next();
        
        for (int n = 2; n <=10; n += 2) {
            System.out.println(n + ". " + message);
        }
    }
}
