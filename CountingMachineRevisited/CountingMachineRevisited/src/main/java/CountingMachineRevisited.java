
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CountingMachineRevisited {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Count from: ");
        int from = keyboard.nextInt();
        System.out.println("Count to: ");
        int to = keyboard.nextInt();
        System.out.println("Count by: ");
        int by = keyboard.nextInt();

        for (int n = from; n <= to; n += by) {
            System.out.println(n);
        }
        System.out.println();
    }
}
