
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AddingValuesInALoop {

    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        int entry;
        int total;
        entry = 1;
        total = 0;
        

        System.out.println("I will add up the numbers you give me.");
        
        while (entry != 0) {
            System.out.println("Number: "); entry = keyboard.nextInt();
            total += entry;
            
            if (entry !=0) {
                System.out.println("The total so far is " + total);
            }
        }
        System.out.println("");
        System.out.println("The total is " + total + ".");
    }
}
