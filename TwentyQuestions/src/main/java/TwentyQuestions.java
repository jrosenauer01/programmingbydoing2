
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class TwentyQuestions {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        String type = "";
        String answer = "";

        System.out.println("TWO QUESTIONS!");
        System.out.println("Think of an object, and I'll try to guess it.");
        System.out.println("");

        System.out.println("Question 1) Is it animal, vegetable or mineral?");
        System.out.print("> ");
        type = keyboard.next();
        System.out.println("");

        System.out.println("Question 2) Is it bigger than a breadbox?");
        System.out.print("> ");
        answer = keyboard.next();
        System.out.println("");

        if (type == "animal" && answer == "yes") {
            System.out.println("moose");
        
        }   else if (type == "animal" && answer == "no") {
            System.out.println("squirrel");
        
        }   else if (type == "vegetable" && answer == "yes") {
            System.out.println("watermelon");
        
        }   else if (type == "vegetable" && answer == "no") {
            System.out.println("carrot");
        
        }   else if (type == "mineral" && answer == "yes") {
            System.out.println("Camaro");
            
        }   else if (type == "mineral" && answer == "no") {
            System.out.println("paper clip");
        }
        
        else {
            System.out.println("Sorry but you messed up this fun game.");
        }
    }
}


        
