
import java.io.File;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SummingThreeNumbersFromAFile {

    public static void main(String[] args) throws Exception {

        int number1, number2, number3, sum;

        Scanner numbers = new Scanner(new File("3nums.txt"));
        number1 = numbers.nextInt();
        number2 = numbers.nextInt();
        number3 = numbers.nextInt();

        numbers.close();

        sum = number1 + number2 + number3;
        System.out.println("Reading numbers from file \"3nums.txt\" . . . done");
        System.out.println(number1 + "+" + number2 + "+" + number3 + " = " + sum);

    }
}
