
import java.io.File;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class DisplayingAFile {
    
    public static void main (String [] args) throws Exception {
        
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println();
        System.out.println("Open which file: ");
        String file = keyboard.next();
        
        Scanner reader = new Scanner (new File(file));
        
        while (reader.hasNext()) {
            String w = reader.nextLine();
            System.out.println(w + " ");
        }
        
        reader.close();
    }
}
