
import java.net.URL;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SimpleWebInput {

    public static void main(String[] args) throws Exception {

        URL mURL = new URL("http://programmingbydoing.com/a/simple-web-input.html");
        Scanner webIn = new Scanner(mURL.openStream());

        String one;

        while (webIn.hasNext()) {
            one = webIn.nextLine();
            System.out.println(one);

        }

        webIn.close();
        
    }
}
