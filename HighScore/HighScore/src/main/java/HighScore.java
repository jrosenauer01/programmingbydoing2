
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class HighScore {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        PrintWriter fileOut;
        int score;
        String name;
        

        try {
            fileOut = new PrintWriter("score.txt");
        } catch (IOException e) {
            System.out.println("Sorry, I cannot open this file 'score.txt' for editing.");
            System.out.println("Maybe the file exists and is read-only?");
            fileOut = null;
            System.exit(1);
        }
        
        System.out.println("You got a high score!!");
        System.out.println("");
        System.out.println("Please enter your score: ");
        score = keyboard.nextInt();
        System.out.println("Pleae enter your name: ");
        name = keyboard.next();
        
        System.out.println();
        System.out.println("Data stored into highscore.txt. Thank you play again.");
        
        fileOut.println("High Score: " + score);
        fileOut.println("Name: " + name);
        
        fileOut.close();
    }
}
