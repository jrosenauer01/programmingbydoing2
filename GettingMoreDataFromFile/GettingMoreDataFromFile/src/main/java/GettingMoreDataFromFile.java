
import java.io.File;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
class Person {

    String name;
    int age;

}

public class GettingMoreDataFromFile {

    public static void main(String[] args) throws Exception {

        Person[] personArray = new Person[1];
        String file;
        Scanner keyboard = new Scanner(System.in);
        int i = 0;

        System.out.println("File to open: ");
        file = keyboard.next();
        File personFile = new File(file);
        Scanner readFile = new Scanner(file);

        while (readFile.hasNext()) {
            personArray[i] = new Person();
            String line = readFile.nextLine();
            //array is only read from, never written to
            String[] personInfo = new String[2];
            personArray[i].name = readFile.next();
            personArray[i].age = readFile.nextInt();
            i++;
        }
        readFile.close();

        System.out.println(personArray[0].name + " is " + personArray[0].age);
        System.out.println(personArray[1].name + " is " + personArray[1].age);
        System.out.println(personArray[2].name + " is " + personArray[2].age);
        System.out.println(personArray[3].name + " is " + personArray[3].age);
        System.out.println(personArray[4].name + " is " + personArray[4].age);

    }
}
