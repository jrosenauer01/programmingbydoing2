
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class KeepGuessing {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Random r = new Random();

        int guess = 0;
        int secretNumber = 1 + r.nextInt(10);

        System.out.println("I have chosen a number between 1 and 10. Try to guess it.");

        do {
            System.out.println("Your guess: ");
            guess = keyboard.nextInt();
            if (guess == secretNumber) {
                System.out.println("Your guess is correct.");
            } else if (guess > secretNumber) {
                System.out.println("Your guess is too high.");
            } else {
                System.out.println("Your guess is too low.");
            }
        } while (guess != secretNumber);
    }
}
