
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class MoreUserInputOfData 
{
        public static void main (String [] args) {
            
            Scanner keyboard = new Scanner(System.in);
            
            String firstName;
            String lastName;
            int grade;
            int studentID;
            String login;
            float GPA;
            
            System.out.println("Please enter the following information so I can sell it for a profit!");
            
            System.out.println("First Name: ");firstName = keyboard.next();
            System.out.println("Last Name: ");lastName = keyboard.next();
            System.out.println("Grade (9-12: ");grade = keyboard.nextInt();
            System.out.println("Student ID: ");studentID = keyboard.nextInt();
            System.out.println("Login: ");login = keyboard.next();
            System.out.println("GPA (0.0 - 4.0): "); GPA = keyboard.nextFloat();
            
            System.out.println("Your information:" +
                   "\nLogin: " + login +      
                   "\nID: " + studentID +
                   "\nName:" + lastName + ", " + firstName +
                   "\nGPA: " + GPA +
                   "\nGrade: "+ grade);
        }
}
