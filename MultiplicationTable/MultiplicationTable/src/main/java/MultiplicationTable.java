/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class MultiplicationTable {
    public static void main (String [] args) {
        for (int i = 0; i < 13; i++) {
            if (i == 0) {
                System.out.println("x | 1\t2\t3\t4\t5\t6\t7\t8\t9");
                System.out.println("==+===================================================");
            }
            System.out.println(i + " | ");
            for (int n = 1; n< 10; n++) {
                System.out.println(i * n + "\t");
            }
            System.out.println("\n");
        }
    }
}
