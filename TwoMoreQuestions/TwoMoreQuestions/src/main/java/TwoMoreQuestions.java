
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class TwoMoreQuestions {

    public static void main(String[] args) {

        String location = "";
        String status = "";

        Scanner keyboard = new Scanner(System.in);

        System.out.println("TWO MORE QUESTIONS, BABY!");
        System.out.println("Think of something and I'll try to guess it!");

        System.out.println("Question 1) Does it stay inside, outside, or both?");
        location = keyboard.next();
        System.out.println("Question 2) Is it a living thing?");
        status = keyboard.next();

        if (location == "inside" && status == "yes") {
            System.out.println("Then what else could you be thinking of besides a python?!");
        }
        if (location == "inside" && status == "no") {
            System.out.println("Obviously the non-living, inside/outside thing on your mind is a dead ant.");
        }
        if (location == "outside" && status == "yes") {
            System.out.println("Then what else could you be thinking of besides a python?!");
        }
        if (location == "outside" && status == "no") {
            System.out.println("Then what else could you be thinking of besides a python?!");
        }
        if (location == "both" && status == "yes") {
            System.out.println("Whatever happens");
        }
        if (location == "both" && status == "no") {
            System.out.println("Whatever happens I guess...");
        }

    }
}
