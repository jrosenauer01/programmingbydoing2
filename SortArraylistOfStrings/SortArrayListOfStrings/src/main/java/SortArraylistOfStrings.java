
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SortArraylistOfStrings {
    public static void main(String[] args) {

        ArrayList<String> arrlist = new ArrayList<String>();

        
        arrlist.add("ask");
        arrlist.add("not");
        arrlist.add("what");
        arrlist.add("your");
        arrlist.add("country");
        arrlist.add("can");
        arrlist.add("do");
        arrlist.add("for");
        arrlist.add("but");
        arrlist.add("kennedy");
        arrlist.add("john");

        /*Unsorted List*/
        System.out.println("Before Sorting:");
        for (String counter : arrlist) {
            System.out.println(counter);
        }

        /* Sort statement*/
        Collections.sort(arrlist);

        /* Sorted List*/
        System.out.println("After Sorting:");
        for (String counter : arrlist) {
            System.out.println(counter);

        }
    }
}
