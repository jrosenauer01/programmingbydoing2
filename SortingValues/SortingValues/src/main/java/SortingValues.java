/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class SortingValues {

    public static void main(String[] args) {
        int[] arr = {45, 87, 39, 32, 93, 86, 12, 44, 75, 50};
        int a, b, t;

        // Display the original (unsorted values)
        System.out.print("before: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        // Swap the values around to put them ascending order.

        for (a = 1; a < arr.length; a++) 
            for (b = arr.length - 1; b >= a; b--) {
                if (arr[b - 1] > arr[b]) {
                    t = arr[b - 1];
                    arr[b - 1] = arr[b]; // swap the values in two slots
                    arr[b] = t;
                }
            }

        // Display the values again, now (hopefully) sorted.
        System.out.print("after : ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
