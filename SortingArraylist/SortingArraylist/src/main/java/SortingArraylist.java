
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SortingArraylist {

    public static void main(String[] args) {

        ArrayList<String> arrlist = new ArrayList<String>();

        arrlist.add("10");
        arrlist.add("23");
        arrlist.add("43");
        arrlist.add("38");
        arrlist.add("26");
        arrlist.add("7");
        arrlist.add("15");
        arrlist.add("49");
        arrlist.add("0");
        arrlist.add("22");
        arrlist.add("11");

        /*Unsorted List*/
        System.out.println("Before Sorting:");
        for (String counter : arrlist) {
            System.out.println(counter);
        }

        /* Sort statement*/
        Collections.sort(arrlist);

        /* Sorted List*/
        System.out.println("After Sorting:");
        for (String counter : arrlist) {
            System.out.println(counter);

        }
    }
}
