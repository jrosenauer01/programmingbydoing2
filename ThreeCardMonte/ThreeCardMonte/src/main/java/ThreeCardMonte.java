
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ThreeCardMonte {

    public static void main(String[] args) {

        Random r = new Random();

        Scanner keyboard = new Scanner(System.in);

        int cup1 = 1;
        int cup2 = 2;
        int cup3 = 3;
        int n = 1 + r.nextInt(3);
        int guess = 0;

        System.out.println("You slide up to Fast Eddie's card table and plop down your cash. He glances at you out of the corner of his eye and starts shuffling. He lays down three cards.");
        System.out.println("");
        System.out.println("Which one is the ace?");
        System.out.println("");
        System.out.println("##  ##  ##");
        System.out.println("##  ##  ##");
        System.out.println("1   2   3");

        System.out.println(">");
        guess = keyboard.nextInt();

        if (guess == n) {
            System.out.println("You nailed it!");
        } else {
            System.out.println("Ha Eddie wins again!");
        }
    }
}
