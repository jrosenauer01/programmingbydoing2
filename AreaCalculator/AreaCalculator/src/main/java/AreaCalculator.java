
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AreaCalculator {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println();

        int choice = 1, base, height, length, width, side, radius;

        while (choice != 5) {

            System.out.println("Please choose a shape:");
            System.out.println("1) Triangle");
            System.out.println("2) Rectangle");
            System.out.println("3) Square");
            System.out.println("4) Circle");
            System.out.println("5) Quit");
            System.out.println();

            System.out.println("Which shape: ");
            choice = keyboard.nextInt();
            System.out.println("");

            if (choice == 1) {
                System.out.println("Base: ");
                base = keyboard.nextInt();
                System.out.println("Height: ");
                height = keyboard.nextInt();
                System.out.println();
                System.out.println("The area is " + areaTriangle (base, height));
            }
            if (choice == 2) {
                System.out.println("Length: ");
                length = keyboard.nextInt();
                System.out.println("Width: ");
                width = keyboard.nextInt();
                System.out.println();
                System.out.println("The area is " + areaRectangle(length, width));
            }
            if (choice == 3) {
                System.out.println("Side Length: ");
                side = keyboard.nextInt();
                System.out.println("The area is " + areaSquare(side));

            }
            if (choice == 4) {
                System.out.println("Radius: ");
                radius = keyboard.nextInt();
                System.out.println("The area is " + areaCircle(radius));
            }
            if (choice == 5) {
                System.out.println("Thanks for asking.");
            }
        }
    }

    public static double areaTriangle(int base, int height) {
        double triarea = 0.5 * base * height;
        return triarea;

    }

    public static double areaRectangle(int length, int width) {
        double rectarea = length * width;
        return rectarea;
    }

    public static double areaSquare(int side) {
        double sqarea = side * side;
        return sqarea;
    }

    public static double areaCircle(int radius) {
        double carea = Math.PI * (radius * radius);
        return carea;

    }
}
