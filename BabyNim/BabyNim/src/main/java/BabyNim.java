
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class BabyNim {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();

        int pileA = 3;
        int pileB = 3;
        int pileC = 3;

        System.out.println("A: 3    B: 3    C: 3");
        System.out.println("");
        
        
        while (pileA > 0 || pileB > 0 || pileC > 0) {
            System.out.println("A: " + pileA + "B: " + pileB + "C: " + pileC);
            System.out.println("Choose a pile: ");
            String choice = keyboard.next();
            System.out.println("How many to remove from pile " + choice + ": ");
            int removal = keyboard.nextInt();
            keyboard.nextLine();
            if (choice.equals("A")) {
                pileA -= removal;
            }
            if (choice.equals("B")) {
                pileB -= removal;
            }
            if (choice.equals("C")) {
                pileC -= removal;
            }
            if (pileA <= 0 || pileB <=0 || pileC <=0) {
                System.out.println("All piles are empty. Good job!");
            }
        }
        
        

    }
}
