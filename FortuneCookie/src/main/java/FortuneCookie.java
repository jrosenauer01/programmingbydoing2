
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class FortuneCookie {

    public static void main(String[] args) {
        Random r = new Random();

        int choice = 1 + r.nextInt(6);
        String response = "";

        if (choice == 1) {
            System.out.println("You need to do better at your job.");
        } else if (choice == 2) {
            System.out.println("Learn a new skill.");
        } else if (choice == 3) {
            System.out.println("The groundhog was wrong.");
        } else if (choice == 4) {
            System.out.println("Go find a better man.");
        } else if (choice == 5) {
            System.out.println("You will get a promotion.");
        } else {
            System.out.println("You will get fired.");
        }
    }
}
