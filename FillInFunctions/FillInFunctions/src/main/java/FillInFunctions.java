
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class FillInFunctions {

    public static void main(String[] args) {
        // Fill in the function calls where appropriate.

        System.out.println("Watch as we demonstrate functions.");

        System.out.println();
        System.out.println("I'm going to get a random character from A-Z");
        System.out.println("The character is: " + randChar());

        System.out.println();
        System.out.println("Now let's count from -10 to 10");
        int start, stop;
        start = -10;
        stop = 10;
        counter(start, stop);
        System.out.println("How was that?");

        System.out.println();
        System.out.println("Now we take the absolute value of a number.");
        int x, y = 99;
        x = -10;
        System.out.println("|" + x + "| = " + absolute(x));

        System.out.println();
        System.out.println("That's all.  This program has been brought to you by:");
    }
    
    public static void credits() {

        System.out.println();
        System.out.println("programmed by Graham Mitchell");
        System.out.println("modified by [your name here]");
        System.out.print("This code is distributed under the terms of the standard ");
        System.out.println("BSD license.  Do with it as you wish.");
    }

    public static char randChar() {

        Random r = new Random();

        int numval;
        char charval;

        numval = 1 + r.nextInt(26);
        charval = (char) ('A' + numval);

        return charval;
    }

    public static int counter(int start, int stop) {

        int ctr;
        ctr = start;

        while (ctr <= stop) {
            System.out.print(ctr + " ");
            ctr += 1;
        }

        return ctr;
    }
    
	public static int absolute(int value) {

		int absval;

		if ( value < 0 )
			absval = -value;
		else
			absval = value;

		return absval;
	}
}
