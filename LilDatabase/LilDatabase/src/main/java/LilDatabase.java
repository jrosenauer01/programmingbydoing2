
import java.io.PrintWriter;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
class Student {

    private String name;
    private int grade;
    private double average;

    public Student(String nm, int grd, double avg) {
        name = nm;
        grade = grd;
        average = avg;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public double getAverage() {
        return average;
    }

}

public class LilDatabase {

    public static void main(String[] args) throws Exception {

        Student[] st = new Student[3];
        String n;
        int g;
        double a;
        Scanner scan = new Scanner(System.in);

        PrintWriter fileOut;
        String Student[];
        String name;
        int grade;
        int average;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Enter student 1's name:");
        name = keyboard.next();
        System.out.println("Enter student 2's grade:");
        grade = keyboard.nextInt();
        System.out.println("Enter student 3's average:");
        average = keyboard.nextInt();
        System.out.println();

        System.out.println("The names are: " + st[0].getName() + " " + st[1].getName() + " " + st[2].getName());
        System.out.println("The grades are: " + st[0].getGrade() + " " + st[1].getGrade() + " " + st[2].getGrade());
        System.out.println("The average are: " + st[0].getAverage() + " " + st[1].getAverage() + " " + st[2].getAverage());
    }
}
