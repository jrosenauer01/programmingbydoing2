
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class HowOldAreYou2 {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int age = 0;
        String name;

        System.out.println("Hey, what's your name?"); name = keyboard.next();

        System.out.println("Ok, " + name + ", how old are you?"); age = keyboard.nextInt();

        if (age < 16) {
            System.out.println("You can't drive.");
        } if (age < 18) {
            System.out.println("You can't vote.");
        } if (age < 25) {
            System.out.println("You can't rent a car.");
        } if (age >=25) {
            System.out.println("You can do pretty much anything.");
        }

    }

}


