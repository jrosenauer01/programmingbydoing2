/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class EvennessFunction {
    public static void main (String [] args) {
        for (int i = 1; i < 21; i++) {
            System.out.println("\n" + i + " ");
            if (isEven(i) && isDivisibleBy3(i)) {
                System.out.println("<=");
            } else if (isEven(i)) {
                System.out.println("<");
            } else if (isDivisibleBy3(i)) {
                System.out.println("=");
            }
        }
    }
    public static boolean isEven (int n) {
        boolean is;
        if (n % 2 == 0) {
            is = true;
        } else {
            is = false;
        }
        return is;
    }   
    public static boolean isDivisibleBy3 (int n) {
        boolean is;
        if (n % 3 == 0) {
            is = true;
        } else {
            is = false;
        }
        return is;  
    }
}
