
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class TheForgetfulMachine 

{
        public static void main (String [] args)
        {
            Scanner keyboard = new Scanner(System.in);
            
            String word;
            String secondWord;
            int number;
            int secondNumber;
            
            System.out.println("Give me a word!");
            word = keyboard.next();
            
            System.out.println("Give me a second word!");
            secondWord = keyboard.next();
            
            System.out.println("Great, now your favorite number?");
            number = keyboard.nextInt();
            
            System.out.println("And your second-favorite number?");
            secondNumber = keyboard.nextInt();
            
            System.out.println("Whew! Wasn't that fun?");
        }
}
