
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Adventure1 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        String object = "";
        String location = "";
        String decision = "";

        System.out.println("WELCOME TO MITCHELL'S TINY ADVENTURE!");
        System.out.println("");

        System.out.println("You are in a creepy house! Would you like to go to the \"kitchen\" or \"upstairs\"?");
        System.out.print("> ");
        location = keyboard.next();

        if (location == "kitchen") {
            System.out.println("There is a long countertop with dirty dishes everywhere.  Off to one side there is, as you'd expect, a refrigerator. You may open the \"refrigerator\" or look in a \"cabinet\".");
            object = keyboard.next();
            if (object == "refrigerator") {
                System.out.println("Inside the refrigerator you see food and stuff. It looks pretty nasty. Would you like to eat some of the food? (\"yes\" or \"no\")");
                decision = keyboard.next();
                if (decision == "no") {
                    System.out.println("You die of starvation... eventually.");
                } else {
                    System.out.println("You still die but from food poisoning.");
                }
            } else if (object == "cabinet") {
                System.out.println("You see a bomb in the cabinet. Do you touch it?");
                decision = keyboard.next();
                if (decision == "no") {
                    System.out.println("The bomb's timer has 60 seconds left. RUN!");
                } else  {
                    System.out.println("Bomb goes off. You die.");
                }
            }

        } else {
            System.out.println("Upstairs you see a hallway.  At the end of the hallway is the master \"bedroom\".  There is also a \"bathroom\" off the hallway.  Where would you like to go?");
            location = keyboard.next();
            if (object == "bedroom") {
                System.out.println("You are in a plush bedroom, with expensive-looking hardwood furniture. The bed is unmade.  In the back of the room, the closet door is ajar.  Would you like to open the door? (\"yes\" or \"no\")");
                decision = keyboard.next();
                if (decision == "no") {
                    System.out.println("You will never know what happens....");
                } else {
                    System.out.println("A zombie comes out and you die. Bwahahaha");
                }
            }
        }
    }
}
