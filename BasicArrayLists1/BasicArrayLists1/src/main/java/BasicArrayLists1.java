
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class BasicArrayLists1 {

    public static void main(String[] args) {

        ArrayList<Integer> arrlist = new ArrayList<Integer>();

        arrlist.add(0);
        arrlist.add(1);
        arrlist.add(2);
        arrlist.add(3);
        arrlist.add(4);
        arrlist.add(5);
        arrlist.add(6);
        arrlist.add(7);
        arrlist.add(8);
        arrlist.add(9);
        arrlist.add(10);

        for (Integer number : arrlist) {
            System.out.println("Slot " + number + " contains a -113");
        }

    }

}

