
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AskingQuestions {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int age;
        int heightFT;
        int heightIN;
        double weight;

        System.out.println("How old are you? ");
        age = keyboard.nextInt();

        System.out.println("How tall are you (in feet)? ");
        heightFT = keyboard.nextInt();
        
        System.out.println("How many additional inches are you? ");
        heightIN = keyboard.nextInt();

        System.out.println("How much do you weigh? ");
        weight = keyboard.nextDouble();

        System.out.println("So you're " + age + " old, " + heightFT + " feet " + heightIN + "inches" + weight + " pounds heavy.");
    }
}
