
import java.io.File;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
class Dog {

    String breed;
    int age;
    double weight;
}

public class GettingDataFromFile {

    public static void main(String[] args) throws Exception {
        Dog[] dogArray = new Dog[2];
        String usrFile;
        Scanner kb = new Scanner(System.in);
        int i = 0;

        System.out.print("Which file to open: ");
        usrFile = kb.next();

        File dogFile = new File(usrFile);
        Scanner readFile;
        readFile = new Scanner(dogFile);

        System.out.println("Reading data from " + usrFile + "\n");

        while (readFile.hasNext()) {
            dogArray[i] = new Dog();
            String line = readFile.nextLine();
            String[] dogI = line.split(",");

            dogArray[i].breed = dogI[0];
            dogArray[i].age = Integer.parseInt(dogI[1]);
            dogArray[i].weight = Double.parseDouble(dogI[2]);
            i++;
        }
        readFile.close();

        System.out.println("First dog: " + dogArray[0].breed + ", " + dogArray[0].age + ", " + dogArray[0].weight);
        System.out.println("Second dog: " + dogArray[1].breed + ", " + dogArray[1].age + ", " + dogArray[1].weight);
    }
}
