
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
class Car {

    String make;
    String model;
    int year;
    String license;

}

public class StoringDataInFile {

    public static void main(String[] args) throws Exception {
        Car[] car = new Car[5];
        Scanner kb = new Scanner(System.in);

        for (int i = 0; i < car.length; i++) {
            car[i] = new Car();
            System.out.println("Car " + (i + 1));

            System.out.println("\tMake: ");
            car[i].make = kb.next();
            System.out.println("\tModel: ");
            car[i].model = kb.next();
            System.out.println("\tYear: ");
            car[i].year = kb.nextInt();
            System.out.println("\tLicense: ");
            car[i].license = kb.next();
        }

        System.out.println("To which file do you want to save this information? ");
        String usrFile = kb.next();

        // Creating a new file and writing into that file.
        PrintWriter fileOut = new PrintWriter(new FileWriter(usrFile));

        for (int i = 0; i < car.length; i++) {
            fileOut.println("Car " + (i + 1));
            fileOut.println(car[i].make);
            fileOut.println(car[i].model);
            fileOut.println(car[i].year);
            fileOut.println(car[i].license);
        }
        fileOut.close();
        System.out.println("Data saved.");
    }
}
