/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class NumberPuzzles1 {
    public static void main (String [] args) {
        System.out.println("Here are all the numbers that add up to 60 and subtract to 14:");
        System.out.println("");
        
        for (int i = 0; i < 60; i++) {
            for (int n = 0; n<=60; n++) {
                int c = n + i;
                int d = n - i;
                
                if (c == 60) {
                    if (d == 14) {
                        System.out.println(" ( " + n + ", " + i + " )");
                    }
                }
            }
        }
    }
    
}
