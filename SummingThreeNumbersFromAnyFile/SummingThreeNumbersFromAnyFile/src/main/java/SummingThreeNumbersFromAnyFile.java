
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class SummingThreeNumbersFromAnyFile {

    public static void main(String[] args) throws Exception {

        Scanner keyboard = new Scanner(System.in);

        String file;
        int number1, number2, number3, sum;

        System.out.println("Which file would you like to read from: ");
        file = keyboard.next();
        System.out.println("Reading numbers from file \"" + file + "\"");
        System.out.println();

        Scanner numbers = new Scanner(new File(file));
        number1 = numbers.nextInt();
        number2 = numbers.nextInt();
        number3 = numbers.nextInt();

        numbers.close();

        sum = number1 + number2 + number3;
        System.out.println("number1" + "+" + " + number2" + "+ number3" + " =" + sum);
    }
}
