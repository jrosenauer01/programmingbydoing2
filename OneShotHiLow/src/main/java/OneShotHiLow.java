
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class OneShotHiLow {

    public static void main(String[] args) {

        Random r = new Random();
        Scanner keyboard = new Scanner(System.in);

        int n = 1 + r.nextInt(100);
        int guess = 0;

        System.out.println("I'm thinking of a number between 1-100. Try to guess.");
        guess = keyboard.nextInt();

        if (guess < n) {
            System.out.println("Sorry you are too low. I was thinking of " + n);
        } else if (guess > n) {
            System.out.println("Sorry you are too high. I was thinking of " + n);
        } else {
            System.out.println("You are correct! What are the odds?!");
        }
    }
}
