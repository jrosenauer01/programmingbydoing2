
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class CountingMachine {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Count to: ");
        int m = keyboard.nextInt();

        for (int n = 1; n <= m; n += 1) {
            System.out.println(n);
        }
        System.out.println();
    }
}
