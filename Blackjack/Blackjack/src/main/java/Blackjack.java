
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class Blackjack {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random r = new Random();

        int card = 1;
        String pChoice = "hit";

        System.out.println("Welcome to Mitchell's Blackjack Program");

        int p1 = 2 + r.nextInt(10);
        int p2 = 2 + r.nextInt(10);
        int d1 = 2 + r.nextInt(10);
        int d2 = 2 + r.nextInt(10);
        int pTotal = p1 + p2;
        int dTotal = d1 + d2;

        System.out.println("You drew " + p1 + " and " + p2);
        System.out.println("Your total is " + pTotal);

        System.out.println("The dealer has a " + d2 + " showing and a hidden card.");
        System.out.println("His total is hidden, too.");

        while (pTotal < 22 && pChoice == "hit") {
            pChoice = keyboard.next();

            System.out.println("Would you like to \"hit\" or \"stay\"?");
            pChoice = keyboard.next();

            if (pChoice == "hit") {
                card = 2 + r.nextInt(10);
                pTotal += card;

                System.out.println("You drew a " + card);
                if (pTotal > 21) {
                    System.out.println("Bust!");
                } else {
                    System.out.println("Your total is " + pTotal);
                }
            }

            System.out.println();
        }

        System.out.println("Okay, dealer's turn.");
        System.out.println("His hidden card was a " + d1);
        System.out.println("His total was " + dTotal);
        System.out.println();

        while (dTotal <= 17 && pTotal <= 21) {

            card = 2 + r.nextInt(10);
            dTotal += card;

            System.out.println("Dealer chooses to hit.");
            System.out.println("He draws a " + card);
            if (dTotal > 21) {
                System.out.println("Bust.");
            } else {
                System.out.println("His total is " + dTotal);
            }
            System.out.println();
        }

        if (dTotal <= 21) {
            System.out.println("Dealer stays.");
            System.out.println("");
        }

        if (dTotal > 21) {
            System.out.println("Dealer bust");
        } else {
            System.out.println("Dealer total is " + dTotal);
        }

        if (pTotal > 21) {
            System.out.println("You bust");
        } else {
            System.out.println("Your total is " + pTotal);
        }

        System.out.println();

        if (pTotal > 21) {
            System.out.println("DEALER WINS!");
        } else if (dTotal > 21) {
            System.out.println("YOU WIN!");
        } else if (dTotal > pTotal) {
            System.out.println("DEALER WINS!");
        } else {
            System.out.println("IT'S A TIE, BUT DEALER STILL WINS!");
        }
    }
}
